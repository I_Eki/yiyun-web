import { YijianDataPool, YijianData, readDataList } from './models';

export interface YijianStateInterface {
  hasConnectedToPusher: boolean | null;
  dataList: YijianData[];
  dataPool: YijianDataPool;
}

function state(): YijianStateInterface {
  const dataList = readDataList();

  return {
    hasConnectedToPusher: null,
    dataList,
    dataPool: {},
  };
}

export default state;
