import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { YijianStateInterface } from './state';
import { DATA_LIST, HAS_CONNECTED_TO_PUSHER } from './getters-types';

const getters: GetterTree<YijianStateInterface, StateInterface> = {
  [DATA_LIST]: (ctx) => ctx.dataList,
  [HAS_CONNECTED_TO_PUSHER]: (ctx) => ctx.hasConnectedToPusher,
};

export default getters;
