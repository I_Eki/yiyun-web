/** 数据项列表 */
export const DATA_LIST = 'dataList';

/** pusher连接状态 */
export const HAS_CONNECTED_TO_PUSHER = 'hasConnectedToPusher';
