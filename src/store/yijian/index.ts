import { Module } from 'vuex';
import { StateInterface, NAMESPACE_YIJIAN } from '../index';
import state, { YijianStateInterface } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export const getType = (type: string) => {
  return `${NAMESPACE_YIJIAN}/${type}`;
};

const yijianModule: Module<YijianStateInterface, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default yijianModule;
