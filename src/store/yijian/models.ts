import { getLocalItem, setLocalItem } from 'utils/storage';
import { YIJIAN_LOCAL_KEY_DATALIST } from 'src/constant/yijian';

// ---- Baidu ----

export interface BaiduWordsResultProbability {
  average: number;
  variance: number;
  min: number;
}

export interface BaiduWordResult {
  words?: string;
  probability?: BaiduWordsResultProbability;
}

export interface BaiduParagraphsResult {
  words_result_idx?: number[];
}

export interface BaiduItem {
  direction?: number;
  log_id: number;
  words_result_num: number;
  words_result: BaiduWordResult[];
  paragraphs_result?: BaiduParagraphsResult[];
  language?: number;
  pdf_file_size?: string;
}

// ---- Huawei ----

export interface HuaweiExtractedData {
  contact_info: unknown;
  image_size: unknown;
}

export interface HuaweiWebImageWordsBlockListItem {
  words: string;
  confidence: number;
  location: number[];
  extracted_data?: HuaweiExtractedData;
  height?: number;
  width?: number;
  name?: string;
  phone?: string;
  province?: string;
  city?: string;
  district?: string;
  detail_address?: string;
}

export interface HuaweiWebImageResult {
  words_block_count: number;
  words_block_list: HuaweiWebImageWordsBlockListItem[];
}

export interface HuaweiItem {
  result?: HuaweiWebImageResult;
}

// ---- Tencent ----

export interface TencentCoord {
  X: number;
  Y: number;
}

export interface TencentItemCoord extends TencentCoord {
  Width: number;
  Height: number;
}

export interface TencentDetectedWords {
  Confidence: number;
  Character: string;
}

export interface TencentDetectedWordCoordPoint {
  WordCoordinate: TencentCoord[];
}

export interface TencentTextDetection {
  DetectedText: string;
  Confidence: number;
  Polygon: TencentCoord[];
  AdvancedInfo: unknown;
  ItemPolygon: TencentItemCoord;
  Words: TencentDetectedWords[];
  WordCoordPoint: TencentDetectedWordCoordPoint[];
}

export interface TencentResponse {
  TextDetections: TencentTextDetection[];
  Language: string;
  Angel: number;
  PdfPageSize: number;
  RequestId: string;
}

export interface TencentItem {
  Response: TencentResponse;
}

// ---- Store ----

export type ResultItem = BaiduItem | HuaweiItem | TencentItem;

export type ResultType = 'auto' | 'baidu' | 'huawei' | 'tencent';

export interface PusherData {
  key: string;
  type: ResultType;
  index: number;
  total: number;
  text: string;
}

export interface YijianData {
  key: string;
  type: ResultType;
  item: ResultItem;
  isOrigin?: boolean;
  content?: string;
}

export interface YijianDataPool {
  [key: string]: PusherData[];
}

export interface YijianChangeDataDto {
  index: number;
  content: string;
}

export const resolveYijianData = (item: YijianData, lineSeparator = '\n\n') => {
  if (!item.isOrigin) return item.content || '';

  let resultText = '';

  switch (item.type) {
    // case 'huawei':
    //   resultText =
    //     (item.item as HuaweiItem).result?.words_block_list
    //       .map((block) => block.words)
    //       .join(lineSeparator) || '';
    //   break;
    case 'auto':
    case 'tencent':
      resultText = (item.item as TencentItem).Response.TextDetections.map(
        (td) => td.DetectedText,
      ).join(lineSeparator);
      break;
    case 'baidu':
      const paragraphs = (item.item as BaiduItem).paragraphs_result;
      const wordsResult = (item.item as BaiduItem).words_result;
      if (paragraphs && wordsResult) {
        resultText = paragraphs
          .map((paragraphsResult) => {
            return (
              paragraphsResult.words_result_idx
                ?.map((idx) => wordsResult[idx].words)
                .join('') || ''
            );
          })
          .join(lineSeparator);
      }
      break;
  }

  return resultText;
};

export const readDataList = () => {
  return (getLocalItem(YIJIAN_LOCAL_KEY_DATALIST) as YijianData[]) || [];
};

export const saveDataList = (val: YijianData[]) => {
  setLocalItem(
    YIJIAN_LOCAL_KEY_DATALIST,
    val.filter((item) => !item.isOrigin),
  );
};
