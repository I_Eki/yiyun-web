/** 修改pusher连接状态 */
export const CHANGE_PUSHER_CONNECT_STATUS = 'changePusherConnectStatus';

/** 格式化分包数据 */
export const RESOLVE_DATA = 'resolveData';

/** 移除指定位置的数据项 */
export const REMOVE_AT = 'removeAt';

/** 修改对应位置数据项的文字内容 */
export const CHANGE_DATA = 'changeData';

/** 保存数据项列表（将会修改当前已编辑的数据项） */
export const SAVE_DATA_LIST = 'saveDataList';
