import { MutationTree } from 'vuex';
import { PusherData, saveDataList, YijianChangeDataDto } from './models';
import type { YijianStateInterface } from './state';
import {
  selectDataAt,
  getCurrentContent,
} from 'pages/Yijian/components/TextDataList/useSelect';
import {
  CHANGE_PUSHER_CONNECT_STATUS,
  RESOLVE_DATA,
  REMOVE_AT,
  CHANGE_DATA,
  SAVE_DATA_LIST,
} from './mutation-types';

const mutation: MutationTree<YijianStateInterface> = {
  [CHANGE_PUSHER_CONNECT_STATUS](state, payload: boolean | null) {
    state.hasConnectedToPusher = payload;
  },
  [RESOLVE_DATA](state, payload: PusherData) {
    const { key, type, total } = payload;
    let { text } = payload;
    if (total > 1) {
      let bubble = state.dataPool[key];
      if (bubble) {
        bubble.push(payload);
        if (bubble.length >= total) {
          bubble.sort((a, b) => a.index - b.index);
          text = bubble.map((section) => section.text).join('');
        }
      } else {
        text = '';
        bubble = state.dataPool[key] = [payload];
      }
    }

    if (text) {
      const result = JSON.parse(text);
      const item = {
        key,
        type,
        isOrigin: true,
        item: result,
      };
      state.dataList.push(item);
      delete state.dataPool[key];

      selectDataAt(state.dataList.length - 1, item);
    }
  },
  [REMOVE_AT](state, payload: number) {
    const { dataList } = state;
    dataList.splice(payload, 1);
    state.dataList = dataList;
    saveDataList(dataList);
  },
  [CHANGE_DATA](state, payload: YijianChangeDataDto) {
    const item = state.dataList[payload.index];
    if (!item) return;
    const isFirstTime = item.isOrigin;
    item.isOrigin = false;
    item.content = payload.content;
    if (!isFirstTime) saveDataList(state.dataList);
  },
  [SAVE_DATA_LIST](state) {
    const data = getCurrentContent();
    if (!data) return;
    const item = state.dataList[data.index];
    if (!item) return;
    item.isOrigin = false;
    item.content = data.content;
    saveDataList(state.dataList);
  },
};

export default mutation;
