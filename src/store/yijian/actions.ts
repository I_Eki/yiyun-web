import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { YijianStateInterface } from './state';

const actions: ActionTree<YijianStateInterface, StateInterface> = {
  someAction(/* context */) {
    // your code
  },
};

export default actions;
