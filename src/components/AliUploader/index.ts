import { createUploaderComponent } from 'quasar';
import { computed, ref } from 'vue';
import { uploadImage } from 'src/api/yijian/oss';

// export a Vue component
export default createUploaderComponent({
  name: 'AliUploader',

  props: {
    uploadDir: String,
  },

  emits: ['failed'],

  injectPlugin({ emit, helpers, props }) {
    const promises = ref<Promise<any>[]>([]);
    const workingThreads = ref(0);
    let abortPromises = false;

    const isUploading = computed(() => {
      return workingThreads.value > 0;
    });

    const isBusy = computed(() => {
      return promises.value.length > 0;
    });

    function abort() {}

    function upload() {
      const queues = helpers.queuedFiles.value.slice();
      helpers.queuedFiles.value = [];
      emit('uploading');
      queues.forEach((file) => uploadFile(file));
    }

    function uploadFile(file: File) {
      workingThreads.value++;

      const promise = uploadImage(file, props.uploadDir);
      promises.value.push(promise);

      const failed = (err: Error) => {
        if (helpers.isAlive() === true) {
          promises.value = promises.value.filter((p) => p !== promise);

          if (promises.value.length === 0) {
            abortPromises = false;
          }

          helpers.queuedFiles.value.push(file);
          helpers.updateFileStatus(file, 'failed');

          emit('failed', err, file);
          workingThreads.value--;
        }
      };

      promise
        .then((res) => {
          if (abortPromises === true) {
            failed(new Error('Aborted'));
          }

          promises.value = promises.value.filter((p) => p !== promise);
          workingThreads.value--;
          helpers.updateFileStatus(file, 'uploaded');
          (<any>file).__url = `![${res?.name || ''}](${res?.url || ''})`;

          if (promises.value.length < 1) {
            emit('uploaded', res);
          }
        })
        .catch(failed);
    }

    return {
      isUploading,
      isBusy,

      abort,
      upload,
    };
  },
});
