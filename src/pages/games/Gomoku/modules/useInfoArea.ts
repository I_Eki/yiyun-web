import { computed } from 'vue';
import { blackColor, whiteColor } from './useBoard';
import { ctx, height, scale, state, GameState, isNextBlack, manual, width, isMeBlack } from './useGame';

interface Size {
  width: number;
  height: number;
}

// 信息区域背景色
const infoAreaColor = '#C3BA85';
// 信息区域宽度
export const infoAreaWidth = computed(() => width.value - height.value);
export const btnSize = computed<Size>(() => {
  return {
    width: 60 * scale.value,
    height: 24 * scale.value,
  };
});
// 按钮背景色
const buttonColor = '#FFF1BD';
// 轮次提示文字颜色
const stepTextColor = '#297F87';
// 结束提示文字颜色
const finalTextColor = '#5D8233';
// 悔棋按钮背景色
const undoButtonColor = '#5D8233';

/** 清空信息区域 */
export const clearInfoArea = () => {
  ctx.value!.fillStyle = infoAreaColor;
  ctx.value!.fillRect(height.value, 0, infoAreaWidth.value, height.value);
};

/** 绘制控制按钮 */
export const drawButton = () => {
  const { width: bw, height: bh } = btnSize.value;
  ctx.value!.clearRect(
    height.value + infoAreaWidth.value / 2 - bw / 2,
    20 * scale.value,
    bw,
    bh,
  );

  ctx.value!.fillStyle = buttonColor;
  ctx.value!.fillRect(
    height.value + infoAreaWidth.value / 2 - bw / 2,
    20 * scale.value,
    bw,
    bh,
  );

  let btnContent = '';
  switch (state.value) {
    case GameState.NOT_JOIN:
      btnContent = '连接中';
      break;
    case GameState.JOINED:
      btnContent = '准备';
      break;
    case GameState.PREPARED:
      btnContent = '准备中';
      break;
    case GameState.PLAYING:
      btnContent = '暂停';
      break;
    case GameState.PAUSED:
      btnContent = '继续';
      break;
    case GameState.FINISHED:
      btnContent = '重来';
      break;
  }

  ctx.value!.font = '16px';
  ctx.value!.fillStyle = '#000';
  const text = ctx.value!.measureText(btnContent);
  ctx.value!.fillText(
    btnContent,
    height.value + infoAreaWidth.value / 2 - text.width / 2,
    20 * scale.value + bh / 2 + 4,
  );
};

/** 绘制轮次提示文字 */
export const drawStepTipText = () => {
  ctx.value!.fillStyle = infoAreaColor;
  ctx.value!.fillRect(
    height.value,
    30 * scale.value + btnSize.value.height,
    infoAreaWidth.value,
    16,
  );

  const contents = ['该 ', isNextBlack.value === isMeBlack.value ? '己方' : '对方', ' 落子'];
  const texts = contents.map((content) => ctx.value!.measureText(content));
  const len = texts.reduce((p, c) => (p += c.width), 0);
  ctx.value!.font = '16px';
  let stepLen = 0;
  contents.forEach((content, i) => {
    stepLen += texts[i - 1]?.width || 0;
    ctx.value!.fillStyle =
      i === 1 ? (isNextBlack.value ? blackColor : whiteColor) : stepTextColor;
    ctx.value!.fillText(
      content,
      height.value + infoAreaWidth.value / 2 - len / 2 + stepLen,
      30 * scale.value + btnSize.value.height + 8,
    );
  });
};

/** 绘制结束提示文字 */
export const drawFinalText = (hasWinner = true) => {
  ctx.value!.fillStyle = infoAreaColor;
  ctx.value!.fillRect(
    height.value,
    50 * scale.value + btnSize.value.height,
    infoAreaWidth.value,
    16,
  );

  if (state.value !== GameState.FINISHED) return;

  if (!hasWinner) {
    const content = '所有人都是输家!';
    const text = ctx.value!.measureText(content);
    ctx.value!.fillStyle = finalTextColor;
    ctx.value!.fillText(
      content,
      height.value + infoAreaWidth.value / 2 - text.width / 2,
      50 * scale.value + btnSize.value.height + 8,
    );
    return;
  }

  const contents = [isNextBlack.value ? '白' : '黑', ' 方胜利\u0021'];
  const texts = contents.map((content) => ctx.value!.measureText(content));
  const len = texts.reduce((p, c) => (p += c.width), 0);
  ctx.value!.font = '16px';
  let stepLen = 0;
  contents.forEach((content, i) => {
    stepLen += texts[i - 1]?.width || 0;
    ctx.value!.fillStyle =
      i === 0 ? (isNextBlack.value ? whiteColor : blackColor) : finalTextColor;
    ctx.value!.fillText(
      content,
      height.value + infoAreaWidth.value / 2 - len / 2 + stepLen,
      50 * scale.value + btnSize.value.height + 8,
    );
  });
};

/** 绘制悔棋按钮 */
export const drawUndoButton = () => {
  const { width: bw, height: bh } = btnSize.value;
  ctx.value!.fillStyle = infoAreaColor;
  ctx.value!.fillRect(
    height.value + infoAreaWidth.value / 2 - bw / 2,
    90 * scale.value,
    bw,
    bh,
  );

  if (manual.value.length < 1) return;
  ctx.value!.fillStyle = undoButtonColor;
  ctx.value!.fillRect(
    height.value + infoAreaWidth.value / 2 - bw / 2,
    90 * scale.value,
    bw,
    bh,
  );

  const btnContent = '悔棋';

  ctx.value!.font = '16px';
  ctx.value!.fillStyle = '#000';
  const text = ctx.value!.measureText(btnContent);
  ctx.value!.fillText(
    btnContent,
    height.value + infoAreaWidth.value / 2 - text.width / 2,
    90 * scale.value + bh / 2 + 4,
  );
};

/** 渲染信息区域 */
export const renderInfoArea = () => {
  clearInfoArea();

  drawButton();
  drawStepTipText();
  drawUndoButton();
};
