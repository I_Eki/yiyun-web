import { ref } from 'vue';
import { personID, roomID, socket } from './useSocket';
import { userName } from './useUserInfo';

export enum ChatType {
  MESSAGE,
  NOTICE,
}

export interface ChatMessage {
  name: string;
  text: string;
  type: ChatType;
}

export const chatMessages = ref<ChatMessage[]>([]);

export const sendingText = ref('');

/** 发送信息 */
export const sendMessage = (text: string) => {
  socket.emit('chat', {
    id: personID,
    name: userName.value,
    roomID,
    text,
  });
};

export const handleSendBtnClick = () => {
  if (!sendingText.value) return;

  sendMessage(sendingText.value);
  sendingText.value = '';
};
