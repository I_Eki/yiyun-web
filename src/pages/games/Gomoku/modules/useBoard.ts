import { computed } from 'vue';
import { ctx, height, ways, manual, isNextBlack } from './useGame';

export interface Piece {
  x: number;
  y: number;
  val: boolean | undefined;
}

// 棋盘背景色
export const boardColor = '#FFBD35';
// 棋格线颜色
export const lineColor = '#000';
// 白子颜色
export const whiteColor = '#fff';
// 黑子颜色
export const blackColor = '#000';

// 棋子大小
export const pieceSize = computed(() => {
  const w = (height.value * 0.9) / (ways.value - 1);
  return {
    side: height.value * 0.05,
    width: w,
    height: w,
  };
});

/** 清空棋盘区域 */
const clearBoard = () => {
  ctx.value!.fillStyle = boardColor;
  ctx.value!.fillRect(0, 0, height.value, height.value);
};

export const clearAt = (x: number, y: number) => {
  const { width: pw, height: ph, side: ps } = pieceSize.value;

  // 覆盖背景
  ctx.value!.fillStyle = boardColor;
  ctx.value!.fillRect(ps + pw * (x - 0.5), ps + ph * (y - 0.5), pw, ph);
};

/** 在棋盘上指定坐标处绘制 */
export const drawAt = (x: number, y: number, isBlack?: boolean) => {
  const { width: pw, height: ph, side: ps } = pieceSize.value;
  const hpw = pw / 2;

  // 绘制内容
  ctx.value!.beginPath();
  if (isBlack === false) {
    // 绘制白子
    ctx.value!.fillStyle = whiteColor;
    ctx.value!.arc(ps + x * pw, ps + y * ph, hpw * 0.75, 0, Math.PI * 2);
    ctx.value!.fill();
  } else if (isBlack === true) {
    // 绘制黑子
    ctx.value!.fillStyle = blackColor;
    ctx.value!.arc(ps + x * pw, ps + y * ph, hpw * 0.75, 0, Math.PI * 2);
    ctx.value!.fill();
  } else {
    // 无棋子，绘制棋盘格线
    ctx.value!.strokeStyle = lineColor;
    const m = ps;
    const n = ps + hpw;
    const t = height.value - m;
    if (x === 0 && y === 0) {
      // 左上角
      ctx.value!.moveTo(m, m);
      ctx.value!.lineTo(n, m);
      ctx.value!.moveTo(m, m);
      ctx.value!.lineTo(m, n);
    } else if (x === 0 && y === ways.value - 1) {
      // 左下角
      ctx.value!.moveTo(m, t);
      ctx.value!.lineTo(n, t);
      ctx.value!.moveTo(m, t);
      ctx.value!.lineTo(m, t - hpw);
    } else if (x === ways.value - 1 && y === 0) {
      // 右上角
      ctx.value!.moveTo(t, m);
      ctx.value!.lineTo(t, n);
      ctx.value!.moveTo(t, m);
      ctx.value!.lineTo(t - hpw, m);
    } else if (x === ways.value - 1 && y === ways.value - 1) {
      // 右下角
      ctx.value!.moveTo(t, t);
      ctx.value!.lineTo(t - hpw, t);
      ctx.value!.moveTo(t, t);
      ctx.value!.lineTo(t, t - hpw);
    } else if (x === 0) {
      // 第一列
      const s = n + (y - 1) * ph;
      ctx.value!.moveTo(m, s);
      ctx.value!.lineTo(m, s + ph);
      ctx.value!.moveTo(m, s + hpw);
      ctx.value!.lineTo(n, s + hpw);
    } else if (x === ways.value - 1) {
      // 最后一列
      const s = n + (y - 1) * ph;
      ctx.value!.moveTo(t, s);
      ctx.value!.lineTo(t, s + ph);
      ctx.value!.moveTo(t, s + hpw);
      ctx.value!.lineTo(t - hpw, s + hpw);
    } else if (y === 0) {
      // 第一行
      const s = n + (x - 1) * pw;
      ctx.value!.moveTo(s, m);
      ctx.value!.lineTo(s + pw, m);
      ctx.value!.moveTo(s + hpw, m);
      ctx.value!.lineTo(s + hpw, m + hpw);
    } else if (y === ways.value - 1) {
      // 最后一行
      const s = n + (x - 1) * pw;
      ctx.value!.moveTo(s, t);
      ctx.value!.lineTo(s + pw, t);
      ctx.value!.moveTo(s + hpw, t);
      ctx.value!.lineTo(s + hpw, t - hpw);
    } else {
      // 中间十字形
      const p = n + (x - 1) * pw;
      const q = n + (y - 1) * ph + hpw;
      ctx.value!.moveTo(p, q);
      ctx.value!.lineTo(p + pw, q);
      ctx.value!.moveTo(p + hpw, q - hpw);
      ctx.value!.lineTo(p + hpw, q + hpw);
    }
    ctx.value!.stroke();
  }
};

/** 初始化渲染棋盘 */
export const initBoard = () => {
  manual.value = [];
  isNextBlack.value = true;

  clearBoard();

  for (let i = 0; i < ways.value; i++) {
    for (let j = 0; j < ways.value; j++) {
      drawAt(i, j);
    }
  }
};
