import { ref, watch } from 'vue';
import { initBoard, Piece, pieceSize } from './useBoard';
import {
  renderInfoArea,
  drawButton,
  drawFinalText,
  infoAreaWidth,
  btnSize,
} from './useInfoArea';
import {
  prepare,
  reqPause,
  reqContinue,
  reqPlayAgain,
  reqUndo,
  luozi,
} from './useSocket';

/** 游戏状态 */
export enum GameState {
  /** 未加入房间 */
  NOT_JOIN,
  /** 已加入房间 */
  JOINED,
  /** 准备中 */
  PREPARED,
  /** 正在游戏 */
  PLAYING,
  /** 已暂停 */
  PAUSED,
  /** 已结束 */
  FINISHED,
}

// canvas内loading
export const innerLoadingVisible = ref(false);
// 是否横屏模式
export const isHorizon = ref(true);
// 缩放比例
export const scale = ref(0);
// canvas宽度
export const width = ref(0);
// canvas高度
export const height = ref(0);
// 棋盘路数
export const ways = ref(0);
// 下一手是否为黑方落子
export const isNextBlack = ref(true);
// 当前玩家是否执黑
export const isMeBlack = ref(true);
// 棋谱
export const manual = ref<Piece[]>([]);

// canvas绘画对象
export const ctx = ref<CanvasRenderingContext2D>();

// 游戏状态
export const state = ref<GameState>(GameState.NOT_JOIN);

// 渲染游戏
export const renderGame = () => {
  if (!ctx.value) return;

  initBoard();
  renderInfoArea();
};

export const handleCanvasClick = (e: MouseEvent) => {
  const { layerX: x, layerY: y } = e as any;
  const { side: ps, width: pw, height: ph } = pieceSize.value;
  if (
    state.value === GameState.PLAYING &&
    isNextBlack.value === isMeBlack.value &&
    x > ps - pw / 2 &&
    x < height.value - pw / 2 &&
    y > ps - ph / 2 &&
    y < height.value - ph / 2
  ) {
    // 点击了棋盘区域，计算点击的坐标
    const rx = Math.floor((x - ps + pw / 2) / pw);
    const ry = Math.floor((y - ps + ph / 2) / ph);
    if (manual.value.some((i) => i.x === rx && i.y === ry)) return;
    luozi({
      x: rx,
      y: ry,
      val: isMeBlack.value,
    });
  } else if (
    x > height.value + infoAreaWidth.value / 2 - btnSize.value.width / 2 &&
    x < height.value + infoAreaWidth.value / 2 + btnSize.value.width / 2 &&
    y > 20 * scale.value &&
    y < 20 * scale.value + btnSize.value.height
  ) {
    // 点击了按钮
    if (state.value === GameState.JOINED) {
      prepare();
    } else if (state.value === GameState.PLAYING) {
      reqPause();
    } else if (state.value === GameState.PAUSED) {
      reqContinue();
    } else if (state.value === GameState.FINISHED) {
      reqPlayAgain();
    }
    drawButton();
  } else if (
    state.value === 0 &&
    x > height.value + infoAreaWidth.value / 2 - btnSize.value.width / 2 &&
    x < height.value + infoAreaWidth.value / 2 + btnSize.value.width / 2 &&
    y > 90 * scale.value &&
    y < 90 * scale.value + btnSize.value.height
  ) {
    // 悔棋
    if (manual.value.length < 1) return;
    reqUndo();
  }
};

export default function () {
  watch(state, () => {
    drawButton();
    drawFinalText();
  });
}
