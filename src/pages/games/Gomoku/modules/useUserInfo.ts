import { initSocket } from './useSocket';
import { getUUID } from 'src/utils/common';
import { ref } from 'vue';

const STORE_KEY_USER_NAME = '__gomoku_user_name__';

export const nameDialogVisible = ref(false);

export const userName = ref('');

(function () {
  let name = localStorage.getItem(STORE_KEY_USER_NAME);
  if (!name) {
    nameDialogVisible.value = true;
    name = '用户' + getUUID().slice(0, 6);
  }
  userName.value = name;
})();

export const handleConfirmName = () => {
  localStorage.setItem(STORE_KEY_USER_NAME, userName.value);
  nameDialogVisible.value = false;
  initSocket();
};

export default function() {}
