import { userName } from './useUserInfo';
import { onMounted } from 'vue';
import { useRoute } from 'vue-router';
import { getUUID } from 'src/utils/common';
import { io, Socket } from 'socket.io-client';
import { Notify, Loading, Dialog } from 'quasar';
import { GameState, isNextBlack, manual, state, isMeBlack, innerLoadingVisible } from './useGame';
import { clearAt, drawAt, Piece, initBoard } from './useBoard';
import { drawStepTipText, drawUndoButton, drawButton, drawFinalText } from './useInfoArea';
import { ChatMessage, chatMessages } from './useChat';

export enum DirectiveType {
  STATE_CHANGED,
  PLAYED,
}

interface StateChangedData {
  target: GameState;
}

interface PlayedData {
  X: number;
  Y: number;
  isBlack: boolean;
}

interface LuoziBackDto {
  isNextBlack: boolean;
  piece: Piece;
}

interface FinishBackDto {
  hasWinner: true | null;
  isNextBlack: boolean;
}

interface StateChangedDirective {
  type: DirectiveType.STATE_CHANGED;
  data: StateChangedData;
}

interface PlayedDirective {
  type: DirectiveType.PLAYED;
  data: PlayedData;
}

export type Directive = StateChangedDirective | PlayedDirective;

Notify.setDefaults({
  position: 'top',
  timeout: 2000,
});

interface Person {
  id: string;
  isBlack: boolean;
  prepared: boolean;
}

export enum RoomState {
  DEFAULT,
  PLAYING,
  PAUSING,
  FINISHED,
}

interface Room {
  id: string;
  state: RoomState;
  isNextBlack?: boolean;
  manual?: Piece[];
  persons: Person[];
}

interface JoinBackData {
  code: 0 | -1 | -2;
  roomID?: string;
  message?: string;
  isMeBlack: boolean;
  person?: Person;
  room?: Room;
}

interface JoinBackDto {
  code: 0 | -1 | -2;
  data: JoinBackData;
}

interface ReqBackDto {
  id: string;
  name: string;
}

const STORE_KEY_PERSON_ID = '__gomoku_person_id__';

const getPersonID = () => {
  let id = localStorage.getItem(STORE_KEY_PERSON_ID);
  if (id) return id;
  id = getUUID();
  localStorage.setItem(STORE_KEY_PERSON_ID, id);
  return id;
};

export const personID = getPersonID();

export let socket: Socket;
export let roomID = '';

export const prepare = () => {
  Loading.show();
  socket.emit('prepare', personID);
};

export const reqPause = () => {
  innerLoadingVisible.value = true;
  socket.emit('pause', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const abortPause = () => {
  socket.emit('abort-pause', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const agreePause = () => {
  socket.emit('agree-pause', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

export const reqContinue = () => {
  innerLoadingVisible.value = true;
  socket.emit('continue', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const abortContinue = () => {
  socket.emit('abort-continue', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const agreeContinue = () => {
  socket.emit('agree-continue', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

export const reqPlayAgain = () => {
  innerLoadingVisible.value = true;
  socket.emit('play-again', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const abortPlayAgain = () => {
  socket.emit('abort-play-again', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

const agreePlayAgain = () => {
  socket.emit('agree-play-again', {
    id: personID,
    name: userName.value,
    roomID,
  });
};

export const reqUndo = () => {};

export const luozi = (piece: Piece) => {
  Loading.show();
  socket.emit('luozi', {
    roomID,
    piece,
  });
};

/** 初始化通信连接 */
export const initSocket = () => {
  socket = new (io as any)();

  socket.on('connect', () => {
    socket.emit('join', {
      id: personID,
      name: userName.value,
      roomID,
    });
  });

  socket.on('disconnect', () => {
    socket.emit('exited', {
      id: personID,
      name: userName.value,
      roomID,
    });
  });

  socket.on('joined', ({ data }: JoinBackDto) => {
    if (data && data.code === 0) {
      state.value = GameState.JOINED;
      isMeBlack.value = data.isMeBlack;

      if (data.room) {
        switch (data.room.state) {
          case RoomState.DEFAULT:
            state.value = GameState.JOINED;
            break;
          case RoomState.PLAYING:
            state.value = GameState.PLAYING;
            break;
          case RoomState.PAUSING:
            state.value = GameState.PAUSED;
            break;
          case RoomState.FINISHED:
            state.value = GameState.FINISHED;
            break;
        }
        isNextBlack.value = data.room.isNextBlack === undefined ? true : data.room.isNextBlack;
        drawButton();
        drawStepTipText();
        data.room.manual?.forEach(piece => {
          drawAt(piece.x, piece.y, piece.val);
        });
      }

      Notify.create({
        message: '已加入房间',
        type: 'positive',
      });
    } else {
      Notify.create({
        message: data.message || '网络错误',
        type: 'negative',
      });
    }
  });

  socket.on('prepared', () => {
    state.value = GameState.PREPARED;
    Loading.hide();
  });

  socket.on('start', () => {
    state.value = GameState.PLAYING;
    drawStepTipText();
    Loading.hide();
  });

  socket.on('req-pause', ({ id, name }: ReqBackDto) => {
    if (id === personID) return;
    Dialog.create({
      title: '提示',
      message: `玩家 ${name} 请求暂停，是否同意？`,
      ok: '同意',
      cancel: '不同意',
    }).onCancel(abortPause).onOk(agreePause);
  });

  socket.on('abort-pause', () => {
    innerLoadingVisible.value = false;
  });

  socket.on('paused', () => {
    state.value = GameState.PAUSED;
    innerLoadingVisible.value = false;
  });

  socket.on('req-continue', ({ id, name }: ReqBackDto) => {
    if (id === personID) return;
    Dialog.create({
      title: '提示',
      message: `玩家 ${name} 请求继续，是否同意？`,
      ok: '同意',
      cancel: '不同意',
    }).onCancel(abortContinue).onOk(agreeContinue);
  });

  socket.on('abort-continue', () => {
    innerLoadingVisible.value = false;
  });

  socket.on('continue', () => {
    state.value = GameState.PLAYING;
    innerLoadingVisible.value = false;
  });

  socket.on('req-play-again', ({ id, name }: ReqBackDto) => {
    if (id === personID) return;
    Dialog.create({
      title: '提示',
      message: `玩家 ${name} 请求重来，是否同意？`,
      ok: '同意',
      cancel: '不同意',
    }).onCancel(abortPlayAgain).onOk(agreePlayAgain);
  });

  socket.on('abort-play-again', () => {
    innerLoadingVisible.value = false;
  });

  socket.on('play-again', () => {
    state.value = GameState.PLAYING;
    initBoard();
    drawFinalText(false);
    drawStepTipText();
    innerLoadingVisible.value = false;
  });

  socket.on('luozi', ({ isNextBlack: inb, piece }: LuoziBackDto) => {
    const { x, y, val } = piece;
    drawAt(x, y, val);

    isNextBlack.value = inb;
    drawStepTipText();

    Loading.hide();
  });

  socket.on('finish', ({ hasWinner, isNextBlack: inb }: FinishBackDto) => {
    state.value = GameState.FINISHED;
    isNextBlack.value = inb;
    drawFinalText(!!hasWinner);
  });

  socket.on('undo', () => {
    const undoStep = manual.value.pop()!;
    isNextBlack.value = !isNextBlack.value;
    clearAt(undoStep.x, undoStep.y);
    drawAt(undoStep.x, undoStep.y);
    drawStepTipText();
    drawUndoButton();
    Loading.hide();
  });

  socket.on('chat', (msg: ChatMessage) => {
    chatMessages.value.push(msg);
  });
};

export default function() {
  const route = useRoute();

  onMounted(() => {
    roomID = route.query.roomID?.toString() || '';

    if (!socket) {
      initSocket();
    }
  });
}
