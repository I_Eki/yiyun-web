import { ref } from 'vue';
import { doc } from '../../../modules/useAce';

interface Theme {
  title: string;
  key: string;
  type: 'light' | 'dark';
}

/** 编辑器可用主题 */
export const themes: Theme[] = [
  {
    title: 'Ambiance',
    key: 'ambiance',
    type: 'dark',
  },
  {
    title: 'Chaos',
    key: 'chaos',
    type: 'dark',
  },
  {
    title: 'Chrome',
    key: 'chrome',
    type: 'light',
  },
  {
    title: 'Clouds',
    key: 'clouds',
    type: 'light',
  },
  {
    title: 'CloudsMidnight',
    key: 'clouds_midnight',
    type: 'dark',
  },
  {
    title: 'Cobalt',
    key: 'cobalt',
    type: 'dark',
  },
  {
    title: 'CrimsonEditor',
    key: 'crimson_editor',
    type: 'light',
  },
  {
    title: 'Dawn',
    key: 'dawn',
    type: 'light',
  },
  {
    title: 'Dracula',
    key: 'dracula',
    type: 'dark',
  },
  {
    title: 'Dreamweaver',
    key: 'dreamweaver',
    type: 'light',
  },
  {
    title: 'Eclipse',
    key: 'eclipse',
    type: 'light',
  },
  {
    title: 'Github',
    key: 'github',
    type: 'light',
  },
  {
    title: 'Gob',
    key: 'gob',
    type: 'dark',
  },
  {
    title: 'Gruvbox',
    key: 'gruvbox',
    type: 'dark',
  },
  {
    title: 'IdleFingers',
    key: 'idle_fingers',
    type: 'dark',
  },
  {
    title: 'Iplastic',
    key: 'iplastic',
    type: 'light',
  },
  {
    title: 'Katzenmilch',
    key: 'katzenmilch',
    type: 'light',
  },
  {
    title: 'KrTheme',
    key: 'kr_theme',
    type: 'dark',
  },
  {
    title: 'Kuroir',
    key: 'kuroir',
    type: 'light',
  },
  {
    title: 'Merbivore',
    key: 'merbivore',
    type: 'dark',
  },
  {
    title: 'MerbivoreSoft',
    key: 'merbivore_soft',
    type: 'dark',
  },
  {
    title: 'MonoIndustrial',
    key: 'mono_industrial',
    type: 'dark',
  },
  {
    title: 'Monokai',
    key: 'monokai',
    type: 'dark',
  },
  {
    title: 'NordDark',
    key: 'nord_dark',
    type: 'dark',
  },
  {
    title: 'OneDark',
    key: 'one_dark',
    type: 'dark',
  },
  {
    title: 'PastelOnDark',
    key: 'pastel_on_dark',
    type: 'dark',
  },
  {
    title: 'SolarizedDark',
    key: 'solarized_dark',
    type: 'dark',
  },
  {
    title: 'SolarizedLight',
    key: 'solarized_light',
    type: 'light',
  },
  {
    title: 'Sqlserver',
    key: 'sqlserver',
    type: 'light',
  },
  {
    title: 'Terminal',
    key: 'terminal',
    type: 'dark',
  },
  {
    title: 'Textmate',
    key: 'textmate',
    type: 'light',
  },
  {
    title: 'Tomorrow',
    key: 'tomorrow',
    type: 'light',
  },
  {
    title: 'TomorrowNight',
    key: 'tomorrow_night',
    type: 'dark',
  },
  {
    title: 'TomorrowNightBlue',
    key: 'tomorrow_night_blue',
    type: 'dark',
  },
  {
    title: 'TomorrowNightBright',
    key: 'tomorrow_night_bright',
    type: 'dark',
  },
  {
    title: 'TomorrowNightEighties',
    key: 'tomorrow_night_eighties',
    type: 'dark',
  },
  {
    title: 'Twilight',
    key: 'twilight',
    type: 'dark',
  },
  {
    title: 'VibrantInk',
    key: 'vibrant_ink',
    type: 'dark',
  },
  {
    title: 'Xcode',
    key: 'xcode',
    type: 'light',
  },
];
/** 主题列表项 */
export const themeOptions = ref<Theme[]>(themes);
/** 编辑器亮色主题 */
export const lightThemes: Theme[] = themes.filter(
  (theme) => theme.type === 'light',
);
/** 编辑器暗色主题 */
export const darkThemes: Theme[] = themes.filter(
  (theme) => theme.type === 'dark',
);
/** 当前主题 */
export const currentTheme = ref<Theme>(themes.find((t) => t.key === 'chrome')!);

export const handleSwitchThemeClick = (item: Theme) => {
  // 动态引入主题文件
  import(`ace-builds/src-noconflict/theme-${item.key}.js`).then(() => {
    if (!doc.value) return;

    doc.value.setTheme(`ace/theme/${item.key}`);
  });
};

export const handleFilterThemes = (
  val: string,
  update: (callback: () => void) => void,
) => {
  update(() => {
    themeOptions.value = themes.filter((t) => t.key.startsWith(val));
  });
};
