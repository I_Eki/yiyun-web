import { icon } from 'src/utils/yui';

/** 获取拼接完成的html文件内容 */
export const getHTMLContent = (fileName: string, innerHTML: string) => {
  return `
    <!DOCTYPE html>
    <html lang="zh">
      <head>
        <title>${fileName}</title>
    
        <meta charset="utf-8">
        <meta name="description" content="Created by Yijian">
        <meta name="format-detection" content="telephone=no">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width<% if (ctx.mode.cordova || ctx.mode.capacitor) { %>, viewport-fit=cover<% } %>">

        <link rel="icon" type="image/ico" href="${icon}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/5.1.0/github-markdown-light.css" integrity="sha512-1d9gwwC3dNW3O+lGwY8zTQrh08a41Ejci46DdzY1aQbqi/7Qr8Asp4ycWPsoD52tKXKrgu8h/lSpig1aAkvlMw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/themes/prism-okaidia.min.css" integrity="sha512-mIs9kKbaw6JZFfSuo+MovjU+Ntggfoj8RwAmJbVXQ5mkAX5LlgETQEweFPI18humSPHymTb5iikEOKWF7I8ncQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <style>
          body {
            padding: 16px;
          }

          .markdown-body > p:not(:has(em, img)) {
            text-indent: 2em;
          }
        </style>
      </head>
      <body>
        <div class="markdown-body">
          ${innerHTML}
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/prism.min.js" integrity="sha512-/Swpp6aCQ0smuZ+zpklJqMClcUlvxhpLf9aAcM7JjJrj2waCU4dikm3biOtMVAflOOeniW9qzaNXNrbOAOWFCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-c.min.js" integrity="sha512-8VrjxGFLIkS0mgEmO3p46A5OkqATHhrNVwyv2V7yUeZrk1jmSDuI3SOEpC9XHEHUWEOsfzzcJeBlUkee9lKGrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-csharp.min.js" integrity="sha512-NohR0FaHlz90CtXG/syKPx9DUzzupUltzyiY0RrNsOryPY3NIusNThXSiusO+tIrTfgqRBvANPqBej/O+FKf2w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-python.min.js" integrity="sha512-AKaNmg8COK0zEbjTdMHJAPJ0z6VeNqvRvH4/d5M4sHJbQQUToMBtodq4HaV4fa+WV2UTfoperElm66c9/8cKmQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-visual-basic.min.js" integrity="sha512-e/iNIg1sCEIKN2h9oy55RpKZT1iJ3HFyoQpJaQmuNqokIULt/ryZwOtGd/pQjQ4BWfxq0Pf+pFh9l7SR0h+PgQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-nginx.min.js" integrity="sha512-Yqm+Wop3IysUyB7d2nO/M5EGljFQ3hJjh1dXgggnqIbRJ9CiydVYq4LjV5yU3iTCHrsrTVTQt7xDaX0NOmvBhQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-mongodb.min.js" integrity="sha512-wCYqRRvZqkWlD8so5MyMosXB4ejuc6HmFXM60n7kDDtGMzqaj18alQy+q0rncu/fQAgz/6zEH/+/tRUANewHNw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-yaml.min.js" integrity="sha512-/oDzFMYD7kk0uSmERPYzNTGBREQ2mZGYoorj8utUwL4ZWlOuvEWZx8ZJlhsC8XJEnUTKkXXnRh+Wp6ywIx1Qfg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-json.min.js" integrity="sha512-QXFMVAusM85vUYDaNgcYeU3rzSlc+bTV4JvkfJhjxSHlQEo+ig53BtnGkvFTiNJh8D+wv6uWAQ2vJaVmxe8d3w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-json5.min.js" integrity="sha512-MbfYOMnSmsdss5g+EHR4wFPGhXX5zQvQk2bVMW+y20PdSj1ir6aFrnLm3S7elwQuv9YaRoyMJFT+U5kQqyTj/Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-jsonp.min.js" integrity="sha512-N2q92ytGio5pTnPWBUQCvDTpj2LIQ3ZwUiRJN6KAMIm+3yw16tHDu8FIA4YK6A6PKFVZlrbOj8gj3L2Z/1UTpA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-bash.min.js" integrity="sha512-ZqfG//sXQwAA7DOArFJyMmZQ3knKe+0ft3tPQZPvDPJR04IatmhVO5pTazVV+fLVDYSy28PhoBeUj5wxGRiGAA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-coffeescript.min.js" integrity="sha512-70hT5k/W5Yo5GHXx0m6hyTHxXx2tqGHtL/TSfBfAMTqk4o2nXAoUeBap6F4zHmqWkjto7k+B05nrcdHelvQ3XQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-typescript.min.js" integrity="sha512-uOw7XYETzS/DPmmirpP5UCMihSDNMeyTS965J0/456OSPfxn9xEtHHjj5Q/5WefVdqyMfN/afmQnNpZd/tpkcA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-sql.min.js" integrity="sha512-sijCOJblSCXYYmXdwvqV0tak8QJW5iy2yLB1wAbbLc3OOIueqymizRFWUS/mwKctnzPKpNdPJV3aK1zlDMJmXQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-lua.min.js" integrity="sha512-3XbHZgyq1WzT6xlH6SxUh9R+P7/vF5UeWET1AH2ESRzWQUfJ78C3CGoXvw2ubFrcX9le6e7M05tUGmPw2bp46Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-less.min.js" integrity="sha512-lL5HvfIycntK04Iiai/VTsyuj7mvDkhz9k+cA8fqXr932s4jLJ1YwplIs6Dhpw0pzVwAe1jGe8sGwbyuHG44QQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-scss.min.js" integrity="sha512-aczOaJ+mB9uGT6dMJbDaUsS2PWG+XII+1ypFQ0L22Z132V6kMM6m70pQssXsPAFmLI5xkgx/hknBuUuJIJKZfA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-css-extras.min.js" integrity="sha512-+db9XAI8OLHEfiCPMsPf59W+SbpiebsWh63wrM1Wh0NQZD12cvuJI6KWEalHf+o+kS22Dc43bIAsJhvcQAPBew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.27.0/components/prism-diff.min.js" integrity="sha512-QHFQ4+gqPck5hEy4UhfKynXlq3U58GuSQli0/+i6o1AcC/6ht6PtwhtkNIurLfshVbaXkPDXdVp8p/eGXfRKIA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      </body>
    </html>  
  `;
};
