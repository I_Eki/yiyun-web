import { doc } from '../../modules/useAce';
import 'ace-builds/src-noconflict/ext-searchbox';

export const handleSearch = () => {
  doc.value?.execCommand('find');
};

export const handleReplace = () => {
  doc.value?.execCommand('replace');
};

export default function() {}
