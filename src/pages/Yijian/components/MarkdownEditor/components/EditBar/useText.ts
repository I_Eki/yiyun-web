import AceIns, { Ace } from 'ace-builds';
import { computed } from 'vue';
import { doc } from '../../modules/useAce';

const boldReg = /^\s*\*\*(.*)\*\*\s*$/;
const italicReg = /^\s*\*(.*)\*\s*$/;
const strikeThroughReg = /^\s*~~(.*)~~\s*$/;
export const hasSelection = computed(() => !doc.value?.selection.isEmpty());

/**
 * 替换选中文字
 * @param replaceExec 替换方法或目标字符串
 * @param needFocus 在替换后是否自动聚焦
 */
export const replaceSelectedText = (
  replaceExec: string | ((text: string) => string),
  needFocus = true,
) => {
  if (!doc.value || !hasSelection.value) return;

  const text = doc.value.getSelectedText();
  if (typeof replaceExec === 'string') {
    doc.value.session.replace(doc.value.getSelectionRange(), replaceExec);
  } else {
    const replacement = replaceExec(text);
    doc.value.session.replace(doc.value.getSelectionRange(), replacement);
  }
  if (needFocus) delayFocus();
};

/**
 * 在光标处替换或插入文字
 * @param replaceExec 替换方法或目标字符串
 * @param needFocus 替换后是否自动聚焦
 * @param isInsert 是否在光标处插入
 */
export const replaceCursorLineText = (
  replaceExec: string | ((text: string) => string),
  needFocus = true,
  isInsert = false,
) => {
  if (!doc.value) return;

  const { row, column } = doc.value.getCursorPosition();
  const text = doc.value.session.getLine(row);

  let replacement = text;
  if (typeof replaceExec === 'string') {
    replacement = replaceExec;
  } else {
    replacement = replaceExec(text);
  }

  doc.value.session.replace(
    new (AceIns.require('ace/range').Range)(
      row,
      isInsert ? column : 0,
      row,
      isInsert ? column : Number.MAX_VALUE,
    ) as Ace.Range,
    replacement,
  );

  if (needFocus) delayFocus();
};

/**
 * 切换指定标记符具有的文字风格
 * @param reg 匹配正则
 * @param tag 标记符（单边）
 */
export const toggleTextStyle = (reg: RegExp, tag: string) => {
  if (!doc.value) return;

  if (hasSelection.value) {
    replaceSelectedText((text) => {
      text = text.replace(/^\s+|\s+$/g, '');
      const replacement = reg.test(text)
        ? text.match(reg)?.[1] || ''
        : `${tag}${text}${tag}`;
      return ` ${replacement} `;
    });
  } else {
    replaceCursorLineText((text) => {
      return reg.test(text)
        ? text.match(reg)?.[1] || ''
        : `${tag}${text}${tag}`;
    });
  }
};

/**
 * 在行开头添加标记符
 * @param tag 标记符
 * @param count 标记符数量
 * @param isReplace 是否需要替换原有标记符
 */
export const appendAtStart = (tag: string, count = 1, isReplace = false) => {
  replaceCursorLineText((text) => {
    if (isReplace) {
      text = text.replace(new RegExp(`^\\${tag}*\\s+`), '');
    }
    return count < 1 ? text : `${Array(count).fill(tag).join('')} ${text}`;
  });
};

/**
 * 在光标处插入文字
 * @param replaceExec 替换方法或目标字符串
 * @param needFocus 在插入后是否自动聚焦
 */
export const insertAtCursor = (
  replaceExec: string | ((text: string) => string),
  needFocus = true,
) => {
  if (!doc.value) return;

  if (hasSelection.value) {
    replaceSelectedText(replaceExec);
  } else {
    replaceCursorLineText(replaceExec, needFocus, true);
  }
};

export const toggleBold = () => {
  toggleTextStyle(boldReg, '**');
};

export const toggleItalic = () => {
  toggleTextStyle(italicReg, '*');
};

export const toggleStrikeThrough = () => {
  toggleTextStyle(strikeThroughReg, '~~');
};

export const indentQuote = () => {
  appendAtStart('>');
};

export const selectedFirstToUpper = () => {
  replaceSelectedText((text) => {
    return text.replace(/^(?<=[^a-zA-Z]*)[a-z]/, (p) => p.toUpperCase());
  });
};

export const indentHeader = (level: number) => {
  appendAtStart('#', level, true);
};

export const insertBreakLine = () => {
  insertAtCursor(`\n\n${Array(12).fill('-').join('')}\n\n`);
};

export const delayFocus = () => {
  setTimeout(() => {
    doc.value?.focus();
  }, 200);
};

export default function () {}
