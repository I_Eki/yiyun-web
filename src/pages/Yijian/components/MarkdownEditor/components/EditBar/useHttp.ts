import { insertAtCursor } from './useText';
import { reactive, ref, watch, nextTick } from 'vue';
import { doc } from '../../modules/useAce';
import type { QTreeNode } from 'quasar';
import { uslugify } from 'utils/markdown';
import { Ace } from 'ace-builds';

interface MDLink {
  title: string;
  url: string;
}

interface AnchorPoint {
  key: string;
  id: string;
  label: string;
  level: number;
  content: string;
  expandable?: boolean;
  parentNode?: AnchorPoint;
  children?: AnchorPoint[];
}

export const anchorsTreeData = ref<AnchorPoint[]>([]);
export const anchorsTreeMenuVisible = ref(false);

export const insertLink = reactive<MDLink>({
  title: '',
  url: '',
});

export const handleInsertImageLink = (file: unknown) => {
  insertAtCursor((<any>file).__url as string);
};

export const handleInsertLink = () => {
  const linkCode = `[${insertLink.title}](${insertLink.url})`;
  insertAtCursor(linkCode);

  insertLink.title = '';
  insertLink.url = '';
};

export const getAnchorPointsTree = (cm: Ace.Editor) => {
  const anchors: AnchorPoint[] = [];

  let lastNode: AnchorPoint;
  const headReg = /^#+ /;
  cm.session.getLines(0, Number.MAX_VALUE).forEach((line) => {
    if (!headReg.test(line)) return;
    let level = 0;
    const label = line.replace(headReg, (p) => {
      level = p.trim().length;
      return '';
    });
    const anchor: AnchorPoint = {
      key: Math.random().toString().slice(2),
      id: uslugify(line),
      label,
      level,
      content: line,
      expandable: true,
    };

    if (!lastNode) {
      anchors.push(anchor);
    } else if (lastNode.level === anchor.level) {
      if (lastNode.parentNode) {
        anchor.parentNode = lastNode.parentNode;
        lastNode.parentNode.children!.push(anchor);
      } else {
        anchors.push(anchor);
      }
    } else if (lastNode.level < anchor.level) {
      anchor.parentNode = lastNode;
      if (lastNode.children) {
        lastNode.children.push(anchor);
      } else {
        lastNode.children = [anchor];
      }
    } else {
      while (lastNode.parentNode && lastNode.parentNode.level >= anchor.level) {
        lastNode = lastNode.parentNode;
      }

      if (lastNode.parentNode) {
        anchor.parentNode = lastNode.parentNode;
        lastNode.parentNode.children!.push(anchor);
      } else {
        anchors.push(anchor);
      }
    }

    lastNode = anchor;
  });

  return anchors;
};

export const handleAnchorClick = (node: QTreeNode) => {
  anchorsTreeMenuVisible.value = false;

  if (!node.label) return false;
  const label = node.label || '';
  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
  const auchor = `[${label}](#${node.id})`;

  nextTick(() => {
    insertAtCursor(auchor);
  });
};

export default function () {
  watch(doc, () => {
    if (!doc.value) return;

    doc.value.on('change', () => {
      anchorsTreeData.value = getAnchorPointsTree(doc.value!);
    });
  });

  return {};
}
