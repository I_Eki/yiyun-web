import { ref, watch } from 'vue';
import { doc } from '../../modules/useAce';

export const canUndo = ref(false);
export const canRedo = ref(false);

export default function () {
  const handleUndo = () => {
    doc.value?.undo();
  };

  const handleRedo = () => {
    doc.value?.redo();
  };

  watch(doc, () => {
    if (!doc.value) return;
    doc.value.on('change', () => {
      const manager = doc.value?.session.getUndoManager() || false;
      canUndo.value = manager && manager.canUndo();
      canRedo.value = manager && manager.canRedo();
    });
  });

  return {
    handleUndo,
    handleRedo,
  };
}
