import { watch, ref, reactive } from 'vue';
import { doc, md } from '../../modules/useAce';
import { copyToClipboard, Notify, exportFile, useQuasar } from 'quasar';
import { htmlText } from '../../modules/useCommon';
import h2c from 'html2canvas';
import JsPDF from 'jspdf';
import './styles/pdf.less';
import { useStore } from 'store';
import { getType } from 'store/yijian';
import { CHANGE_DATA } from 'store/yijian/mutation-types';
import { currentSelectedIndex } from '../../../TextDataList/useSelect';
import { getHTMLContent } from './lib/html';
import { getOSSImageBase64 } from 'src/api/yijian/oss';
import { ImageWorker } from 'src/utils/imageWorker';

export interface DeltaColor {
  r: number;
  g: number;
  b: number;
  a: number;
}

export interface ImageWorkerOptions {
  type: 0 | 1;
  deltaColor: DeltaColor;
  canvasWidth: number;
  canvasHeight: number;
  canvasPageHeight: number;
}

export interface PDFPageImageData {
  data: Uint8ClampedArray;
  heightPer: number;
  width: number;
  height: number;
}

export const pdfDialogVisible = ref(false);
export const pdfSplitterModel = ref(200);
export const previewLoadingVisible = ref(false);
export const deltaColor = reactive<DeltaColor>({
  r: 255,
  g: 255,
  b: 255,
  a: 255,
});
const pdfImageData = ref<ImageData>();
const pdfPageImageDatas = ref<PDFPageImageData[]>([]);
let pdfContainerNode: HTMLElement | null = null;

const a4Width = 595.28; // a4宽度，按72 pix/inch 计算
const a4Height = 842; // a4高度，按72 pix/inch 计算
const phPer = 15.9 / 210; // 横向间距百分比
const pvPer = 12.7 / 297; // 纵向间距百分比
const widthPer = 1 - phPer * 2; // 宽度百分比
const heightPer = 1 - pvPer * 2; // 高度百分比
let ratio = 1; // 缩放比例
let pdVertical = Math.ceil(ratio * a4Height * pvPer); // pdf纵向间距
const contentHeight = a4Height * heightPer; // pdf内容高度

watch(pdfDialogVisible, (val) => {
  if (val) return;
  if (pdfContainerNode) {
    document.body.removeChild(pdfContainerNode);
    pdfContainerNode = null;
  }
});

export const copyFullText = () => {
  const fullText = doc.value?.getValue() || '';
  copyToClipboard(fullText).then(() => {
    Notify.create({
      message: '复制成功！',
      type: 'positive',
      color: 'green-8',
      timeout: 2000,
      position: 'top',
    });
  });
};

/** 将渲染内容中的img标签链接替换为base64内容，避免h2c截图的时候显示空白 */
async function replaceImages(el: HTMLElement) {
  const imgNodes = el.querySelectorAll(':scope img');
  return Promise.all(
    Array.prototype.map.call(imgNodes, async (node: HTMLImageElement) => {
      const base64 = await getOSSImageBase64(node.src);
      node.src = base64;
    }),
  );
}

export const downloadMarkdownFile = () => {
  const content = doc.value?.getValue() || '';
  const status = exportFile(`亦见${Date.now()}.md`, content, {
    encoding: 'utf-8',
    mimeType: 'text/md;charset=utf-8;',
  });
  showDownloadNotify(status);
};

export const downloadHTMLFile = () => {
  const fileName = `亦见${Date.now()}`;
  const innerHTML = md.render(doc.value?.getValue() || '');
  const htmlContent = getHTMLContent(fileName, innerHTML);
  const status = exportFile(`${fileName}.html`, htmlContent, {
    encoding: 'utf-8',
    mimeType: 'text/md;charset=utf-8;',
  });
  showDownloadNotify(status);
};

export const showDownloadNotify = (status: true | Error) => {
  if (status === true) {
    // browser allowed it
    Notify.create({
      message: '下载成功！',
      type: 'positive',
      color: 'green-8',
      timeout: 2000,
      position: 'top',
    });
  } else {
    // browser denied it
    Notify.create({
      message: '下载失败！\n' + status.message,
      type: 'negative',
      color: 'red-8',
      multiLine: true,
      timeout: 2000,
      position: 'top',
    });
  }
};

export default function () {
  const store = useStore();
  const previewCanvasRef = ref<HTMLCanvasElement>();
  const $q = useQuasar();

  const saveCurrentContent = () => {
    if (currentSelectedIndex.value < 0) return;
    const content = doc.value?.getValue() || '';
    store.commit(getType(CHANGE_DATA), {
      index: currentSelectedIndex.value,
      content,
    });
  };

  async function handleDownloadPDFFile() {
    $q.loading.show();

    // 生成临时dom，按A4纸大小渲染出基本样式
    pdfContainerNode = document.createElement('div');
    pdfContainerNode.className = 'pdf-container';

    const wrapper = document.createElement('div');
    wrapper.className = 'pdf-wrapper markdown-body';
    wrapper.innerHTML = htmlText.value;

    pdfContainerNode.appendChild(wrapper);
    await replaceImages(pdfContainerNode);
    document.body.appendChild(pdfContainerNode);
    pdfDialogVisible.value = true;

    $q.loading.hide();
  }

  async function previewPDFPageImages() {
    if (!pdfContainerNode || !previewCanvasRef.value) return;

    previewLoadingVisible.value = true;
    const startTime = Date.now();
    const canvas = await h2c(pdfContainerNode, {
      useCORS: true,
    });
    console.log(`截图耗时: ${Date.now() - startTime}ms.`);

    const ctx = canvas.getContext('2d');
    pdfImageData.value = ctx!.getImageData(0, 0, canvas.width, canvas.height);
    const dataSet = pdfImageData.value.data;

    ratio = canvas.width / (a4Width * widthPer); // 缩放比例
    pdVertical = Math.ceil(ratio * a4Height * pvPer); // pdf纵向间距
    const previewCtx = previewCanvasRef.value.getContext('2d');
    const canvasPageHeight = Math.ceil(ratio * contentHeight); // 映射canvas单页高度

    await new Promise((resolve) => {
      const options: ImageWorkerOptions = {
        type: 0, // 预览渲染
        deltaColor,
        canvasWidth: canvas.width,
        canvasHeight: canvas.height,
        canvasPageHeight,
      };

      const worker = new ImageWorker(
        new URL('src/utils/pdf.worker.js', import.meta.url),
        {
          type: 'module',
        },
      );
      worker.postImageData(options, dataSet);

      worker.onmessage = (e) => {
        if (!previewCanvasRef.value) return;

        const breaks: number[] = JSON.parse(e.data as unknown as string);
        previewCtx!.fillStyle = '#000';
        const previewPageHeight = canvasPageHeight + pdVertical * 2;
        const previewHeight = previewPageHeight * (breaks.length + 1);
        const previewImageData = new ImageData(canvas.width, previewHeight);
        previewCanvasRef.value.width = canvas.width;
        previewCanvasRef.value.height = previewHeight;
        previewCtx!.clearRect(0, 0, canvas.width, previewHeight);

        pdfPageImageDatas.value = [];
        let start = 0;
        const pageLineLen = canvas.width * 4;
        breaks.forEach((line, index) => {
          const end = line * canvas.width * 4;
          const pageData = dataSet.slice(start, end);
          const top = pageLineLen * previewPageHeight * index;
          previewImageData.data.set(pageData, top + pdVertical * pageLineLen);
          const height = line - (breaks[index - 1] || 0);
          pdfPageImageDatas.value.push({
            data: pageData,
            heightPer: height / previewPageHeight,
            width: canvas.width,
            height,
          });
          start = end;

          // 页面开头添加分隔线
          for (let i = 0; i < canvas.width * 2; i++) {
            previewImageData.data.set([0, 0, 0, 255], top + i * 4);
          }
        });

        previewCtx!.putImageData(previewImageData, 0, 0);
        resolve(null);
      };
    });

    previewLoadingVisible.value = false;
  }

  async function downloadPDFFile() {
    if (pdfPageImageDatas.value.length < 1) {
      await previewPDFPageImages();
    }

    // 转换pdf文件
    const startTime = Date.now();
    const pdf = new JsPDF('p', 'pt', 'a4');
    const pdHorizon = ratio * a4Width * phPer; // pdf横向间距
    const contentWidth = a4Width * widthPer; // pdf内容宽度

    pdfPageImageDatas.value.forEach(
      ({ data, heightPer, width, height }, index) => {
        const imageData = new ImageData(width, height);
        imageData.data.set(data);
        pdf.addImage(
          imageData,
          'JPEG',
          pdHorizon / 2,
          pdVertical / 2,
          contentWidth,
          heightPer * contentHeight,
        );
        if (index < pdfPageImageDatas.value.length - 1) {
          pdf.addPage();
        }
      },
    );

    let status: true | Error;
    try {
      pdf.save(`亦见${Date.now()}.pdf`);
      status = true;
    } catch (error) {
      status = error as Error;
    }

    console.log(`生成PDF耗时: ${Date.now() - startTime}ms.`);
    showDownloadNotify(status);
    previewLoadingVisible.value = false;
  }

  watch(
    deltaColor,
    (val) => {
      if (!val) return;

      previewPDFPageImages();
    },
    { deep: true },
  );

  watch(doc, (val) => {
    if (!val) return;

    val.commands.addCommand({
      name: 'save',
      bindKey: {
        win: 'Ctrl-S',
        mac: 'Command-S',
      },
      exec() {
        saveCurrentContent();
      },
    });
  });

  return {
    previewCanvasRef,
    saveCurrentContent,
    previewPDFPageImages,
    handleDownloadPDFFile,
    downloadPDFFile,
  };
}
