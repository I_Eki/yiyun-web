import { QInput } from 'quasar';
import { ref, watch, computed } from 'vue';
import { doc, md, isRealtimeRender } from '../../modules/useAce';
import { htmlText } from '../../modules/useCommon';

export const jumpLineIndex = ref(0);
export const isRowSplit = ref(true);
export const themeSettingsDialogVisible = ref(false);

export const totalLines = computed(() => {
  return doc.value?.session.getLength() || 0;
});

export const jumpToLine = (line: number) => {
  if (!doc.value) return;

  doc.value.scrollToLine(line - 1, false, false, () => void 0);
};

export const toggleRealtimeRender = () => {
  isRealtimeRender.value = !isRealtimeRender.value;
  if (isRealtimeRender.value) renderText();
};

export const renderText = () => {
  if (!doc.value) return;
  htmlText.value = md.render(doc.value.getValue() || '');
};

export const toggleLayout = () => {
  isRowSplit.value = !isRowSplit.value;
  setTimeout(() => {
    doc.value?.resize();
  }, 0);
};

export const handleThemeSettingsClick = () => {
  themeSettingsDialogVisible.value = true;
};

export { isRealtimeRender };

export default function () {
  const jumpLineInput = ref<QInput>();

  watch(jumpLineIndex, (cur) => {
    jumpToLine(Math.min(Math.max(Math.floor(cur), 1), totalLines.value));
  });

  const selectJumpLineInputValue = () => {
    jumpLineInput.value?.select();
  };

  return {
    jumpLineInput,
    selectJumpLineInputValue,
  };
}
