import { ref, onMounted, onUnmounted } from 'vue';
import { debounce, QScrollArea } from 'quasar';
import { createYijianMD } from 'utils/markdown';
import { htmlText } from './useCommon';
// @ts-ignore
import AceIns from 'ace-builds';
import type { Ace } from 'ace-builds';

// 官方解决方案会将所有语言包解决方案都打包
// import 'ace-builds/webpack-resolver';

// 引入markdown语言解析
import 'ace-builds/src-noconflict/mode-markdown';

export const md = createYijianMD();

const renderBoxRef = ref<HTMLElement>();
const renderContainerRef = ref<QScrollArea>();
export const doc = ref<Ace.Editor | null>(null);
export const isRealtimeRender = ref(true);

export default function () {
  let scrollTicking = false;
  let isRenderScrolling = false;
  let isAceScrolling = false;

  const finishAceScroll = debounce(() => {
    isAceScrolling = false;
  }, 300);

  const finishRenderScroll = debounce(() => {
    isRenderScrolling = false;
  }, 300);

  const scrollCallback = (isAceScrolled = false) => {
    if (!scrollTicking) {
      requestAnimationFrame(() => {
        if (!doc.value) return;

        const top = doc.value.renderer.getScrollTop();
        const scrollEl = doc.value.container.querySelector(':scope > .ace_scroller') as HTMLElement;
        if (!scrollEl) return;

        let rows = 0;
        for (let i = 0; i < doc.value.session.getLength(); i++) {
          rows += doc.value.session.getRowLineCount(i);
        }
        const height = rows * parseInt(scrollEl.style.lineHeight);
        const clientHeight = doc.value.container.clientHeight;
        const aceScrollHeight = height - clientHeight;

        if (isAceScrolled) {
          renderContainerRef.value?.setScrollPercentage('vertical', top / aceScrollHeight);
          finishAceScroll();
        } else {
          const { verticalPercentage } = renderContainerRef.value!.getScroll();
          const targetTop = verticalPercentage * (aceScrollHeight);
          doc.value.renderer.scrollTo(0, targetTop);
          finishRenderScroll();
        }

        scrollTicking = false;
      });

      scrollTicking = true;
    }
  };

  const handleRenderContainerScroll = () => {
    if (isAceScrolling) return;
    isRenderScrolling = true;
    scrollCallback();
  };

  const handleAceScroll = () => {
    if (isRenderScrolling) return;
    isAceScrolling = true;
    scrollCallback(true);
  };

  const disableNativeSave = (e: KeyboardEvent) => {
    if (e.ctrlKey && e.key === 'S') {
      if (doc.value && doc.value.isFocused()) {
        e.preventDefault();
      }
    }
  };

  const initAce = () => {
    doc.value = AceIns.edit('editor');
    import('ace-builds/src-noconflict/theme-chrome').then(() => {
      doc.value?.setTheme('ace/theme/chrome');
    });
    doc.value.getSession().setMode('ace/mode/markdown');
    doc.value.setAutoScrollEditorIntoView(true);
    doc.value.setDisplayIndentGuides(false);
    doc.value.setAnimatedScroll(true);
    doc.value.setOption('indentedSoftWrap', false);
    doc.value.session.setUseWrapMode(true);
    doc.value.setWrapBehavioursEnabled(true);
    doc.value.setValue('', -1);

    doc.value.on('change', () => {
      if (!isRealtimeRender.value) return;
      htmlText.value = md.render(doc.value?.getValue() || '');
    });

    doc.value.session.on('changeScrollTop', () => {
      handleAceScroll();
    });
  };

  onMounted(() => {
    initAce();
    window.addEventListener('keydown', disableNativeSave);
  });

  onUnmounted(() => {
    window.removeEventListener('keydown', disableNativeSave);
  });

  return {
    renderBoxRef,
    renderContainerRef,
    scrollCallback,
    handleRenderContainerScroll,
    handleAceScroll,
  };
}
