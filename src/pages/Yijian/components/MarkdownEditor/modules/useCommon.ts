import { ref, computed, onMounted, onUnmounted } from 'vue';

export enum RenderViewMode {
  NORMAL,
  FULL_WINDOW,
  FULL_SCREEN,
}

export const htmlText = ref('');
export const viewMode = ref(RenderViewMode.NORMAL);
export const viewModeClass = computed(() => {
  switch (viewMode.value) {
    case RenderViewMode.FULL_WINDOW:
    case RenderViewMode.FULL_SCREEN:
      return 'full-window';
    case RenderViewMode.NORMAL:
    default:
      return '';
  }
});

export const handleEscKeyDown = (e: KeyboardEvent) => {
  if (e.key !== 'Escape') return;
  viewInNormal();
};

export const handleFullscreenChange = () => {
  if (document.fullscreenElement) return;
  viewInNormal();
  document.removeEventListener('fullscreenchange', handleFullscreenChange);
};

export const viewInFullWindow = () => {
  viewMode.value = RenderViewMode.FULL_WINDOW;
  window.addEventListener('keydown', handleEscKeyDown);
};

export const viewInFullScreen = () => {
  viewMode.value = RenderViewMode.FULL_SCREEN;
  document.documentElement.requestFullscreen();
  document.addEventListener('fullscreenchange', handleFullscreenChange);
};

export const viewInNormal = () => {
  viewMode.value = RenderViewMode.NORMAL;
  window.removeEventListener('keydown', handleEscKeyDown);
};

export default function () {
  const handleF11KeyDown = (e: KeyboardEvent) => {
    if (e.key !== 'F11') return;

    switch (viewMode.value) {
      case RenderViewMode.NORMAL:
        if (e.shiftKey) {
          viewInFullScreen();
        } else {
          viewInFullWindow();
        }
        break;
      case RenderViewMode.FULL_WINDOW:
        viewInFullScreen();
        break;
      case RenderViewMode.FULL_SCREEN:
        viewInNormal();
        break;
    }
    e.preventDefault();
  };

  onMounted(() => {
    window.addEventListener('keydown', handleF11KeyDown);
  });

  onUnmounted(() => {
    window.removeEventListener('keydown', handleF11KeyDown);
  });
}
