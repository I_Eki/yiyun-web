import { doc } from './useAce';

export const updateText = (val: string) => {
  doc.value?.setValue(val);
  doc.value?.focus();
};

export default function () {
  return {};
}
