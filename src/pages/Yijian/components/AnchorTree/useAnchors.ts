import { ref, watch, nextTick } from 'vue';
import { anchorsTreeData } from '../MarkdownEditor/components/EditBar/useHttp';
import type { QTree } from 'quasar';

/** 标识是否展示渲染器侧边锚点树 */
export const isAnchorTreeVisible = ref(false);

export const handleAnchorTreeTriggerClick = () => {
  isAnchorTreeVisible.value = !isAnchorTreeVisible.value;
};

export default function() {
  const anchorTreeRef = ref<QTree | null>(null);

  watch(anchorsTreeData, () => {
    nextTick(() => {
      anchorTreeRef.value?.expandAll();
    });
  });

  return {
    anchorTreeRef,
  };
}
