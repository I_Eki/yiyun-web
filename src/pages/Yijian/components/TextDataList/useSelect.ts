import { resolveYijianData, YijianData } from 'store/yijian/models';
import { ref, nextTick } from 'vue';
import { useStore } from 'store';
import { getType } from 'store/yijian';
import { CHANGE_DATA } from 'store/yijian/mutation-types';
import { doc } from '../MarkdownEditor/modules/useAce';

export const currentSelectedIndex = ref(-1);

export const selectDataAt = (index: number, item: YijianData) => {
  if (!doc.value) return;

  currentSelectedIndex.value = index;
  doc.value.setValue(resolveYijianData(item), -1);
};

export const getCurrentContent = () => {
  const index = currentSelectedIndex.value;
  if (!doc.value || index < 0) return null;

  return {
    index,
    content: doc.value.getValue() || '',
  };
};

export default function () {
  const store = useStore();

  const updateDataContent = (index: number) => {
    const lastIndex = currentSelectedIndex.value;
    if (lastIndex > -1 && lastIndex !== index) {
      const content = doc.value?.getValue() || '';
      nextTick(() => {
        store.commit(getType(CHANGE_DATA), {
          index: lastIndex,
          content,
        });
      });
    }

    currentSelectedIndex.value = index;
  };

  return {
    updateDataContent,
  };
}
