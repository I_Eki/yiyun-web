import { onMounted } from 'vue';
import { default as Pusher } from 'pusher-js';
import { useStore } from 'store';
import { getType } from 'store/yijian';
import {
  CHANGE_PUSHER_CONNECT_STATUS,
  SAVE_DATA_LIST,
  RESOLVE_DATA,
} from 'store/yijian/mutation-types';

export default function () {
  const store = useStore();

  onMounted(() => {
    Pusher.logToConsole = process.env.NODE_ENV === 'development';

    const pusher = new Pusher('2a08da470d790d35b529', {
      cluster: 'ap3',
    });

    const channel = pusher.subscribe('yiyun-serve');

    channel.bind('pusher:subscription_succeeded', function () {
      store.commit(getType(CHANGE_PUSHER_CONNECT_STATUS), true);
    });

    channel.bind('pusher:subscription_error', function () {
      store.commit(getType(CHANGE_PUSHER_CONNECT_STATUS), false);
    });

    channel.bind('transmit', function (data: unknown) {
      store.commit(getType(SAVE_DATA_LIST));
      store.commit(getType(RESOLVE_DATA), data);
    });
  });
}
