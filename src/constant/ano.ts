import { SHA256 } from 'crypto-js';

/** 聊天加密公钥 */
export const PUBLIC_KEY = SHA256('.-../---/...-/./.-/-./-../.--././.-/-.-./.').toString();

/** 聊天图片链接正则 */
export const CHAT_IMAGE_RE = /(?<=\[img\]\()[^()]*(?=\))/g;

/** 聊天图片全值正则 - [img](http://xxx.png) */
export const CHAT_IMAGE_FULL_RE = /\[img\]\([^()]*\)/g;

/** emoji标签匹配标识类型正则 */
export const EMOJI_TAG_CLASS_RE = /(?<=\s*)emoji-[\da-z]+-[\da-z]+/;

/** 聊天记录emoji匹配正则 */
export const CHAT_EMOJI_RE = /\/\((emoji-[\da-z]+-[\da-z]+)\)/g;

/** 聊天用户数据本地储存Key */
export const KEY_CHAT_INFO = '__ano_user_name__';
