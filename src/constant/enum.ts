export enum ChatType {
  MESSAGE,
  NOTICE,
}

export enum UserStatus {
  CREATED,
  ACTIVED,
  DISABLED,
  DELETED,
}
