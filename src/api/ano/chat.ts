import { api } from 'src/boot/axios';
import { UserStatus } from 'src/constant/enum';

export interface User {
  _id: string;
  name: string;
  status: UserStatus;
  createdAt?: number | string;
  updatedAt?: number | string;
}

export interface ChatDto {
  _id: string;
  sender: User;
  persons: string[];
  content: string | string[];
  reference?: ChatDto;
  timestamp: number;
}

export interface QueryChatRecordsDto {
  start: number;
  size: number;
  timestamp: number;
}

export interface QueryChatRecordsResDto {
  start: number;
  size: number;
  end: number;
  finished?: boolean;
  timestamp: number;
  data: ChatDto[];
}

export interface OnlineUser {
  user: User;
  createdAt?: number;
  updatedAt?: number;
}

export async function queryChatRecords(dto: QueryChatRecordsDto) {
  const res = await api.get(`/api/chat/records?start=${dto.start}&size=${dto.size}&timestamp=${dto.timestamp}`);
  if (res.data.code) return null;
  return res.data as QueryChatRecordsResDto;
}

export async function getOnlineUsers() {
  const res = await api.get('/api/chat/users');
  if (res.data.code) return null;
  return res.data as OnlineUser[];
}
