import OSS from 'ali-oss';
import { api } from 'src/boot/axios';
import { getUUID } from 'src/utils/common';

export interface Credentials {
  AccessKeyId: string;
  AccessKeySecret: string;
  Expiration: string;
  SecurityToken: string;
}

export async function getSecret() {
  const res = await api.get('/api/common/alists');
  if (res.data.code) return null;
  return res.data.Credentials as Credentials;
}

export async function uploadImage(data: File, dir = 'mdImages') {
  const credentials = await getSecret();
  if (!credentials) return null;

  const { AccessKeyId, AccessKeySecret, SecurityToken } = credentials;
  const client = new OSS({
    // Bucket所在地域。
    region: 'oss-cn-chengdu',
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。
    // 此处已在后端搭建STS服务，使用临时访问权限。
    accessKeyId: AccessKeyId,
    accessKeySecret: AccessKeySecret,
    stsToken: SecurityToken,
    refreshSTSToken: async () => {
      const res = await getSecret();
      return {
        accessKeyId: res!.AccessKeyId,
        accessKeySecret: res!.AccessKeySecret,
        stsToken: res!.SecurityToken,
      };
    },
    // Bucket名称。
    bucket: 'yiyun-yijian',
  });

  const headers = {
    // 指定该Object被下载时网页的缓存行为。
    'Cache-Control': 'no-cache',
    // 指定该Object被下载时的名称。
    // 'Content-Disposition': 'oss_download.txt',
    // 指定该Object被下载时的内容编码格式。
    // 'Content-Encoding': 'UTF-8',
    // 指定Object的存储类型。
    // 'x-oss-storage-class': 'Standard',
    // 指定Object的访问权限。
    // 'x-oss-object-acl': 'private',
    // 设置Object的标签，可同时设置多个标签。
    // 'x-oss-tagging': 'Tag1=1&Tag2=2',
    // 指定CopyObject操作时是否覆盖同名目标Object。此处设置为true，表示禁止覆盖同名Object。
    // 'x-oss-forbid-overwrite': 'true',
  };

  // 填写OSS文件完整路径和本地文件的完整路径。OSS文件完整路径中不能包含Bucket名称。
  // 如果本地文件的完整路径中未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件。
  const suffix = data.name.match(/[a-zA-Z0-9]+$/)?.[0] || '';
  const result = await client.put(
    `yijian/${dir}/${getUUID()}.${suffix}`,
    data,
    {
      headers,
    },
  );
  return result;
}

export async function getOSSImageBase64(url: string) {
  const res = await api.get('/api/common/oss?url=' + encodeURIComponent(url));
  if (!res) return '';
  return 'data:image/jpeg;base64,' + (res.data.data as string);
}
