import { api } from 'src/boot/axios';
import { UserStatus } from 'src/constant/enum';

export async function createUser(name: string) {
  const res = await api.post('/api/user', { name, status: UserStatus.ACTIVED });
  if (res.data.code) return null;
  return res.data as User;
}

export async function activeUser(_id: string) {
  const res = await api.put('/api/user/active', { _id });
  if (res.data.code) return null;
  return res.data as User;
}
