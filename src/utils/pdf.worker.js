// import JsPDF from 'jspdf';

onmessage = function (e) {
  // 读取流的头部数据信息
  const lenOfLen = 4;
  const buffer = e.data;
  const lenStr = buffer
    .slice(0, lenOfLen)
    .reduce((p, c) => (p += c.toString(2)), '');
  const optionLen = parseInt(lenStr, 2);
  const decoder = new TextDecoder();
  const optionStr = decoder.decode(
    buffer.slice(lenOfLen, optionLen + lenOfLen),
  );
  const options = self.JSON.parse(optionStr);
  const data = buffer.slice(optionLen + lenOfLen);

  console.log(options);
  if (options.type === 0) {
    previewRender(options, data);
  } else if (options.type === 1) {
    // downloadPDF(options, data);
  }
};

function previewRender(options, data) {
  const { deltaColor, canvasWidth, canvasHeight, canvasPageHeight } = options;

  const startTime = Date.now();

  const breaks = [];

  let top = 0; // 单页canvas顶部坐标
  let bottom = canvasPageHeight; // 单页canvas底部坐标

  // 判定指定行是否为空，最大四通道与rgba(255, 255, 255, 255)总误差不超过150
  const isEmptyLine = (start, end) => {
    const line = data.slice(start, end);
    let res = true;
    for (let i = 0; i < line.length; i += 4) {
      res =
        line[i] >= deltaColor.r &&
        line[i + 1] >= deltaColor.g &&
        line[i + 2] >= deltaColor.b &&
        line[i + 3] >= deltaColor.a;
      if (!res) break;
    }
    return res;
  };

  while (top < bottom) {
    const isEnd = bottom >= canvasHeight;

    // 检测单页高度的canvas内容底部是否有截断。如果存在，则递归向上寻找到最近一行空白处，当作单页内容的底部。
    // const pageHeight = bottom - top + 1;
    // const page = data.slice(top * canvasWidth, bottom * canvasWidth);
    let backLines = 0;
    let end = bottom * canvasWidth * 4;
    let start = end - canvasWidth * 4;

    // 判断此行是否为截断处;
    let isBreak = isEnd || isEmptyLine(start, end);
    let currentPageHeight = bottom - top + 1;
    while (!isBreak && backLines < currentPageHeight) {
      backLines++;
      end = start;
      start = end - canvasWidth * 4;
      isBreak = isEmptyLine(start, end);
    }

    // 限制查找到的回退高度大于单页高度，则判定为直接截断。
    if (backLines >= canvasPageHeight) backLines = 0;
    if (backLines > 0) {
      bottom -= backLines;
      currentPageHeight = bottom - top + 1;
    }

    breaks.push(bottom);

    // 修改下一单页内容的顶部和底部坐标
    top = bottom;
    bottom = Math.min(bottom + canvasPageHeight, canvasHeight);
  }

  console.log(`预览耗时: ${Date.now() - startTime}ms.`);

  postMessage(self.JSON.stringify(breaks));
}

// function downloadPDF(options, data) {
//     // 转换pdf文件
//     const startTime = Date.now();
//     const pdf = new JsPDF('p', 'pt', 'a4');
//     const pdHorizon = ratio * a4Width * phPer; // pdf横向间距
//     const contentWidth = a4Width * widthPer; // pdf内容宽度

//     pdfPageImageDatas.value.forEach(
//       ({ data, heightPer, width, height }, index) => {
//         const imageData = new ImageData(width, height);
//         imageData.data.set(data);
//         pdf.addImage(
//           imageData,
//           'JPEG',
//           pdHorizon / 2,
//           pdVertical / 2,
//           contentWidth,
//           heightPer * contentHeight,
//         );
//         if (index < pdfPageImageDatas.value.length - 1) {
//           pdf.addPage();
//         }
//       },
//     );

//     let status;
//     try {
//       pdf.save(`亦见${Date.now()}.pdf`);
//       status = true;
//     } catch (error) {
//       status = error;
//     }

//     console.log(`生成PDF耗时: ${Date.now() - startTime}ms.`);
//     postMessage(self.JSON.stringify({ status }));
// }
