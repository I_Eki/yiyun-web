export class ImageWorker extends Worker {
  postImageData(options: unknown, data: Uint8ClampedArray) {
    const str = JSON.stringify(options);
    const encoder = new TextEncoder();
    const optionBuffer = Uint8ClampedArray.from(encoder.encode(str));
    const optionLen = optionBuffer.length;
    const optionLenByteStr = optionLen.toString(2).padStart(32, '0');

    const lenOfLen = 4;
    const lenNums = [...Array(lenOfLen)].map((_, i) => {
      return parseInt(optionLenByteStr.substring(i * 8, (i + 1) * 8), 2);
    });
    const buffer = new Uint8ClampedArray(4 + optionBuffer.length + data.length);
    buffer.set(lenNums);
    buffer.set(optionBuffer, lenOfLen);
    buffer.set(data, lenOfLen + optionBuffer.length);

    this.postMessage(buffer);
  }
}
