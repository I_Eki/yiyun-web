export function getLocalItem(key: string): any {
  let res = localStorage.getItem(key);
  if (res === null) return res;
  try {
    res = JSON.parse(res);
  } catch (__) {}
  return res;
}

export function setLocalItem(key: string, val: any) {
  const res = JSON.stringify(val);
  localStorage.setItem(key, res);
}
