import MarkdownIt from 'markdown-it';
import MathJax3 from 'markdown-it-mathjax3';
import MIAnchor from 'markdown-it-anchor';
import MITaskLists from 'markdown-it-task-lists';
import uslug from 'uslug';
import type { Options, PluginSimple } from 'markdown-it';
import Prism from 'prismjs';
import 'prismjs/components/prism-c';
import 'prismjs/components/prism-csharp';
import 'prismjs/components/prism-python';
import 'prismjs/components/prism-visual-basic';
import 'prismjs/components/prism-nginx';
import 'prismjs/components/prism-mongodb';
import 'prismjs/components/prism-yaml';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-json5';
import 'prismjs/components/prism-jsonp';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-coffeescript';
import 'prismjs/components/prism-typescript';
import 'prismjs/components/prism-sql';
import 'prismjs/components/prism-lua';
import 'prismjs/components/prism-less';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-css-extras';
import 'prismjs/components/prism-diff';

export const uslugify = (s: string) => uslug(s);

export function createYijianMD(options?: Options) {
  const md = MarkdownIt({
    ...options,
    html: true,
    linkify: true,
    typographer: true,
    highlight: function (str, lang) {
      if (lang && Prism.languages[lang]) {
        try {
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          return `<pre class="language-${lang}"><code class="language-${lang}">${Prism.highlight(
            str,
            Prism.languages[lang],
            lang,
          )}</code></pre>`;
        } catch (__) {}
      }

      return `<pre class="language-${lang}"><code class="language-${lang}">${str}</code></pre>`;
    },
  });

  md.use(MathJax3 as PluginSimple);
  md.use(MIAnchor, {
    slugify: uslugify,
  });
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  md.use(MITaskLists, { enabled: true, label: true, labelAfter: true });

  return md;
}
