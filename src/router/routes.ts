import { RouteRecordRaw } from 'vue-router';
import { getUUID } from 'utils/common';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue'),
      },
    ],
  },

  {
    name: 'Yijian',
    path: '/yijian',
    component: () => import('pages/Yijian/layout/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Yijian/index.vue'),
      },
    ],
  },

  {
    name: 'Games',
    path: '/games',
    component: () => import('src/pages/games/index.vue'),
  },

  {
    name: 'Gomoku',
    path: '/games/gomoku',
    component: () => import('src/pages/games/Gomoku/index.vue'),
    beforeEnter(to, from, next) {
      if (to.query.roomID) {
        next();
      } else {
        next({
          path: to.path,
          query: {
            ...to.query,
            roomID: getUUID(),
          },
        });
      }
    },
  },

  {
    name: 'MPG',
    path: '/mpg',
    component: () => import('src/pages/mpg/index.vue'),
  },

  {
    name: 'Jingwei',
    path: '/jingwei',
    component: () => import('src/pages/Jingwei/index.vue'),
  },

  {
    name: 'ANO',
    path: '/ano',
    component: () => import('src/pages/ANO/index.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
