import AliUploader from 'components/AliUploader/index';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
  app.component('AliUploader', AliUploader);
});
