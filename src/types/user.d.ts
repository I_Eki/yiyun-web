declare interface User {
  _id: string;
  name: string;
  status: number;
  createdAt?: number | string;
  updatedAt?: number | string;
}
