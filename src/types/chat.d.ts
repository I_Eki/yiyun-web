declare interface ChatMessage {
  _id?: string;
  sender?: string;
  name: string;
  text: string | string[];
  reference?: ChatMessage;
  type: number;
}

declare interface ChatMessageResDto {
  _id: string;
  sender: User;
  name: string;
  content: string;
  reference?: ChatMessageResDto;
  type: number;
}
