interface SideMenu {
  title: string,
  caption: string,
  icon?: string,
  link: string,
}

/** 主侧边栏菜单列表 */
export const sideMenuList: SideMenu[] = [
  {
    title: '亦见',
    caption: 'Markdown文档编写工具',
    icon: 'school',
    link: 'yijian',
  },
  {
    title: '小游戏',
    caption: '小游戏demo合集',
    icon: 'sports_esports',
    link: 'games',
  },
  {
    title: '精卫',
    caption: '填海系统',
    icon: 'flutter_dash',
    link: 'jingwei',
  },
  {
    title: 'ANO聊天室',
    caption: 'Anti-Spy',
    icon: 'cruelty_free',
    link: 'ano',
  },
  // {
  //   title: 'Twitter',
  //   caption: '@quasarframework',
  //   icon: 'rss_feed',
  //   link: 'https://twitter.quasar.dev',
  // },
  // {
  //   title: 'Facebook',
  //   caption: '@QuasarFramework',
  //   icon: 'public',
  //   link: 'https://facebook.quasar.dev',
  // },
  // {
  //   title: 'Quasar Awesome',
  //   caption: 'Community Quasar projects',
  //   icon: 'favorite',
  //   link: 'https://awesome.quasar.dev',
  // },
];
